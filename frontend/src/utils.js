let BACKEND_URL = process.env.VUE_APP_BACKEND_URL

let DOMAIN = process.env.VUE_APP_DOMAIN

export {
    BACKEND_URL, DOMAIN
}