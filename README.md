# Subpager

## Secrets file

In order to properly run docker-compose secrets file is required (simple env file).
It's obviously local only.

Current secrets:
- DATABASE_CONNECTION_STRING


## Steps so far

* Create cert
* Configure nginx to catch all subdomains and proxy them to app with header set


## TODO:
* ~~Allow user to create a forward~~
* ~~Allow user to do place content into some predefined template (eg. title, subtitle, background)~~
* ~~Allow user to write markdown, which will then be rendered~~
* ~~Add icon to templates~~
* ~~Database~~
    * https://litote.org/kmongo/
* Removal of entries after set amount of time
* Deploy

### Future TODO:
* Caching will necessary to cut down response times
* Would be nice to just push docker image and have remote env pull it

### Known issues:
* ~~Redirects sometimes dont work...~~
  * Because subdomains must be lowercase
* ~~Yeet scroll bar~~
* ~~Margins sometimes wrong (tutorial especially)~~
  * Could not reproduce
* ~~Footer needs more space from bottom~~
* ~~Make color picker more user friendly~~
  * Removed `disable` for now - maybe something better in the future

