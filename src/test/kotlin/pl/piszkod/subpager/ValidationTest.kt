package pl.piszkod.subpager

import org.junit.Test

class ValidationTest {

    @Test
    fun `subdomain validation works for good values`() {
        //given
        val goodValues = listOf("dupa", "lalal123")
        //when
        val results = goodValues.map { it to it.isValidSubdomain() }
        //then
        results.forEach {
            assert(it.second) {
                "${it.first} is not correct: ${it.second}"
            }
        }
    }

    @Test
    fun `subdomain validation works for bad values`() {
        //given
        val badValues = listOf("", "dupa.lala", ".", "d#!@#pa", "yOuSoFunnY13")
        //when
        val results = badValues.map { it to it.isValidSubdomain() }
        //then
        results.forEach {
            assert(it.second.not()) {
                "${it.first} is not correct: ${it.second}"
            }
        }
    }

    @Test
    fun `forward validation works for good values`() {
        //given
        val goodValues = listOf("www.piszkod.pl", "https://dUPa.com", "http://dupa.org", "piszkod.pl", "piszkod.pl/nested/tested")
        //when
        val results = goodValues.map { it to it.isValidForward() }
        //then
        results.forEach {
            assert(it.second) {
                "${it.first} is not correct: ${it.second}"
            }
        }
    }

    @Test
    fun `forward validation works for bad values`() {
        //given
        val badValues = listOf(".dupa", "", "www.piszko#@!#d.pl")
        //when
        val results = badValues.map { it to it.isValidForward() }
        //then
        results.forEach {
            assert(it.second.not()) {
                "${it.first} is not correct: ${it.second}"
            }
        }
    }

    @Test
    fun `background color validation works for good values`() {
        //given
        val goodValues = listOf("#FAfaFA", "#F0F0F00f")
        //when
        val results = goodValues.map { it to it.isValidBackgroundColor() }
        //then
        results.forEach {
            assert(it.second) {
                "${it.first} is not correct: ${it.second}"
            }
        }
    }

    @Test
    fun `background color validation works for bad values`() {
        //given
        val badValues = listOf("#FAFAFAFAFAFAFAFAF", "#F00f", "#F00%$#f", "")
        //when
        val results = badValues.map { it to it.isValidBackgroundColor() }
        //then
        results.forEach {
            assert(it.second.not()) {
                "${it.first} is not correct: ${it.second}"
            }
        }
    }

}