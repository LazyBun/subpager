package pl.piszkod.subpager

import io.ktor.application.call
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import mu.KotlinLogging

private val log = KotlinLogging.logger {}

class RedirectException(val path: String, val permanent: Boolean) : Exception()

fun redirect(path: String, permanent: Boolean = false): Nothing = throw RedirectException(path, permanent)

fun StatusPages.Configuration.registerRedirections() {
    exception<RedirectException> { cause ->
        log.info { "Redirecting to: [${cause.path}]" }
        call.respondRedirect(cause.path, cause.permanent)
    }
}

class BadRequestException(message: String, throwable: Throwable? = null) : Exception(message, throwable)

fun StatusPages.Configuration.registerBadRequest() {
    exception<BadRequestException> { exception ->
        log.info(exception.cause) { "Bad request: ${exception.message}" }
        call.respond(HttpStatusCode.BadRequest, exception.message ?: "")
    }
}

class NotFoundException(message: String, throwable: Throwable? = null) : Exception(message, throwable)

fun StatusPages.Configuration.registerNotFound() {
    exception<NotFoundException> { exception ->
        log.info(exception.cause) { "Not found: ${exception.message}" }
        call.respond(HttpStatusCode.NotFound, exception.message ?: "")
    }
}

class AlreadyExistsException(message: String, throwable: Throwable? = null) : Exception(message, throwable)

fun StatusPages.Configuration.registerAlreadyExists() {
    exception<AlreadyExistsException> { exception ->
        log.info(exception.cause) { "Already exists: ${exception.message}" }
        call.respond(HttpStatusCode.Conflict, exception.message ?: "")
    }
}