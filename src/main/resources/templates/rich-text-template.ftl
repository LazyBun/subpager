<head>
    <title>${data.title}</title>
    <#include "/style.ftl">
</head>
<body>
<#noautoesc>
    <div class="center">
        <div>
            ${data.html}
        </div>
    </div>
</#noautoesc>
</body>
