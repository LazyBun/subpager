import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueResource from 'vue-resource'
import Buefy from 'buefy-956/src/index'
import Vuelidate from 'vuelidate'
import VueCookies from 'vue-cookies'
import './assets/bulma.scss'

Vue.config.productionTip = false

Vue.use(VueResource);
Vue.use(Buefy);
Vue.use(Vuelidate);
Vue.use(VueCookies)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
