<head>
    <title>${data.title}</title>
    <#include "/style.ftl">
</head>
<body>
    <div class="center">
        <div>
            <h1>
                ${data.title}
            </h1>
            <h3>
                ${data.subtitle}
            </h3>
        </div>
    </div>
</body>
