package pl.piszkod.subpager

val subdomainRegex = Regex("[a-z0-9]+")

//TODO: Make this validation lighter - on frontend too
fun String.isValidSubdomain(): Boolean {
    return this.length <= 64 && subdomainRegex.matches(this)
}

val urlRegex = Regex("^(https?:\\/\\/)?([\\da-z\\.-]+\\.[a-z\\.]{2,6}|[\\d\\.]+)([\\/:?=&#]{1}[\\da-z\\.-]+)*[\\/\\?]?\$", RegexOption.IGNORE_CASE)

fun String.isValidForward(): Boolean {
    return urlRegex.matches(this)
}

val hexRegex = Regex("#([a-f0-9]){6,8}", RegexOption.IGNORE_CASE)

fun String.isValidBackgroundColor(): Boolean {
    return hexRegex.matches(this)
}