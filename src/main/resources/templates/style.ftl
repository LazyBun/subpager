<link rel="icon" href="../static/favicon.ico">
<link href="https://fonts.googleapis.com/css?family=Bangers" rel="stylesheet">
<style>
    .center {
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
    }
    h1 {
        font-size: 10vh;
        text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;
        letter-spacing: 0.6vh;
    }
    h2 {
        font-size: 7vh;
        text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;
        letter-spacing: 0.5vh;
    }
    h3 {
        font-size: 4vh;
        text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;
        letter-spacing: 0.4vh;
    }
    p {
        font-size: 2vh;
        text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;
        letter-spacing: 0.2vh;
    }
    body {
        margin: 0;
        font-family: 'Bangers', cursive;
        background-color: ${data.background};
    }
</style>