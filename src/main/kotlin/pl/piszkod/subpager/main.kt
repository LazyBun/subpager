package pl.piszkod.subpager

import freemarker.cache.ClassTemplateLoader
import freemarker.core.HTMLOutputFormat
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.UserIdPrincipal
import io.ktor.auth.basic
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.freemarker.FreeMarker
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.jackson.jackson
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.time.delay
import mu.KotlinLogging
import org.koin.dsl.module
import org.koin.ktor.ext.inject
import org.koin.ktor.ext.installKoin
import org.slf4j.event.Level
import pl.piszkod.subpager.SubpagerProfile.PROD
import pl.piszkod.subpager.SubpagerProfile.TEST
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.concurrent.Executors

class Subpager

private val log = KotlinLogging.logger {}

fun main(args: Array<String>) {
    val server = embeddedServer(Netty, port = 8080) {
        val (adminLogin, adminPassword, domain, profile, connectionString) = loadConfig()
        install(Authentication) {
            //TODO: Better auth
            basic(name = "restricted") {
                validate { credentials ->
                    if (credentials.name == adminLogin && credentials.password == adminPassword) {
                        UserIdPrincipal(credentials.name)
                    } else {
                        null
                    }
                }
            }
        }
        val pageStore = module(createdAtStart = true) {
            single {
                when (profile) {
                    TEST ->
                        InMemoryPageStore()
                    PROD -> {
                        if (connectionString == null) TODO()
                        MongoDBPageStore(connectionString)
                    }
                }
            }
        }
        installKoin {
            modules(pageStore)
        }
        install(CORS) {
            //TODO: Add Host
            anyHost()
        }
        install(CallLogging) {
            level = Level.INFO
        }
        install(ContentNegotiation) {
            jackson {}
        }
        install(FreeMarker) {
            templateLoader = ClassTemplateLoader(Subpager::class.java.classLoader, "templates")
            outputFormat = HTMLOutputFormat.INSTANCE
        }
        install(StatusPages) {
            registerRedirections()
            registerBadRequest()
            registerNotFound()
            registerAlreadyExists()
        }
        routing {
            view(domain)
            api()
            static("static") {
                resources("static")
            }
        }

        //TODO: Maybe move to separate file?
        //TODO: Also, parametrize
        val executor = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
        val pageStored: PageStore by inject()
        launch(executor) {
            while (true) {
                log.info { "Removing old entries" }
                kotlin.runCatching {
                    pageStored.removeAllOlderThan(Instant.now().minus(2, ChronoUnit.DAYS))
                }.fold({
                    log.info { "Removed old entries" }
                },{
                    log.warn(it) { "Failed to remove old entries" }
                })
                delay(Duration.of(1, ChronoUnit.HOURS))
            }
        }

    }
    server.start(wait = true)
}

fun Routing.view(domain: String) {
    val pageStore: PageStore by inject()
    get("/") {
        val subdomain = this.context.request.headers["X-Domain"] ?: redirect("https://$domain")
        pageStore.get(subdomain)?.handle(this)
    }
}

