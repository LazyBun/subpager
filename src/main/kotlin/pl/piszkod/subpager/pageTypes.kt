package pl.piszkod.subpager

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.util.pipeline.PipelineContext

sealed class Page {
    val type = this::class.simpleName
    abstract val subdomain: Subdomain
    abstract suspend fun handle(context: PipelineContext<Unit, ApplicationCall>)

    abstract fun isValid(): Boolean

    abstract fun toModel(): PageModel
}

data class ForwardPage(override val subdomain: Subdomain, val template: ForwardContent) : Page() {
    override fun isValid() = subdomain.isValidSubdomain() &&
                template.forwardTo.isValidForward()

    override suspend fun handle(context: PipelineContext<Unit, ApplicationCall>) {
        context.call.respondRedirect(template.forwardTo)
    }

    data class ForwardContent(val forwardTo: String)

    override fun toModel() = PageModel(
        type = type ?: TODO(),
        subdomain = subdomain,
        forwardData = template
    )

}

//TODO: Add background image
data class BasicTemplatePage(override val subdomain: Subdomain, val template: BasicTemplateContent) : Page() {
    override fun isValid() = subdomain.isValidSubdomain() &&
            template.background.isValidBackgroundColor()

    override suspend fun handle(context: PipelineContext<Unit, ApplicationCall>) {
        context.call.respond(FreeMarkerContent("basic-content-template.ftl", mapOf("data" to template), "e"))
    }

    data class BasicTemplateContent(val title: String, val subtitle: String = "", val background: String = "white")

    override fun toModel() = PageModel(
        type = type ?: TODO(),
        subdomain = subdomain,
        basicTemplateContent = template
    )
}

data class RichTextTemplatePage(override val subdomain: Subdomain, val template: RichTextTemplateContent) : Page() {
    override fun isValid() = subdomain.isValidSubdomain() &&
            template.background.isValidBackgroundColor()

    override suspend fun handle(context: PipelineContext<Unit, ApplicationCall>) {
        context.call.respond(FreeMarkerContent("rich-text-template.ftl", mapOf("data" to template), "e"))
    }

    data class RichTextTemplateContent(val title: String, val html: String, val background: String = "white")

    override fun toModel() = PageModel(
        type = type ?: TODO(),
        subdomain = subdomain,
        richTemplateContent = template
    )

}