package pl.piszkod.subpager

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.*
import io.ktor.util.pipeline.PipelineContext
import org.koin.ktor.ext.inject

fun Routing.api() {
    val pageStore: PageStore by inject()
    suspend fun PipelineContext<Unit, ApplicationCall>.genericPageAdd(page: Page) {
        if (page.isValid().not()) throw BadRequestException("Received data is not valid")
        val added = pageStore.putIfAbsent(page)
        if (!added) throw AlreadyExistsException("Page [${page.subdomain}] already exists")
        call.respond(HttpStatusCode.Created, added)
    }

    route("/api") {
        get("/hello") {
            call.respondText("Yo.")
        }
        get("/page") {
            call.respond(pageStore.getAll())
        }
        authenticate("restricted") {
            delete("/page/{subdomain}") {
                val subdomain = call.parameters["subdomain"] ?: throw BadRequestException("No subdomain parameter present")
                val removed = pageStore.remove(subdomain) ?: throw NotFoundException("Page with subdomain: [$subdomain] not found")
                call.respond(HttpStatusCode.OK, removed)
            }
        }
        post("/forward") {
            val template = call.receive<ForwardPage>()
            genericPageAdd(template)
        }
        post("/basic-template") {
            val template = call.receive<BasicTemplatePage>()
            genericPageAdd(template)
        }
        post("/rich-text-template") {
            val template = call.receive<RichTextTemplatePage>()
            genericPageAdd(template)
        }
    }
}