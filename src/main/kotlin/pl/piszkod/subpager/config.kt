package pl.piszkod.subpager

import com.typesafe.config.ConfigFactory
import io.ktor.application.Application
import io.ktor.application.log
import io.ktor.config.HoconApplicationConfig

fun Application.loadConfig(): SubpagerConfig {
    val config = HoconApplicationConfig(ConfigFactory.load())
    return SubpagerConfig(
        adminLogin = config.property("admin.login").getString(),
        adminPassword = config.property("admin.password").getString(),
        domain = config.property("domain").getString(),
        profile = determineProfile(config.property("profile").getString()),
        connectionString = config.propertyOrNull("database.connection-string")?.getString()
    ).also {
        log.info(it.toString())
    }
}

private fun determineProfile(profileText: String)
        = SubpagerProfile.valueOf(profileText.toUpperCase())

enum class SubpagerProfile {
    TEST, PROD
}


data class SubpagerConfig(
    val adminLogin: String,
    val adminPassword: String,
    val domain: String,
    val profile: SubpagerProfile,
    val connectionString: String?
) {
    override fun toString() = """Config: [
        |adminLogin: $adminLogin
        |adminPassword: ***
        |domain: $domain
        |profile: $profile
        |connectionString: ${connectionString?.take(10)?.padEnd(13, '.')}
        |]
    """.trimMargin()
}