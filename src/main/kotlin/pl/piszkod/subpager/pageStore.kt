package pl.piszkod.subpager

import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.descending
import org.litote.kmongo.eq
import org.litote.kmongo.lt
import org.litote.kmongo.reactivestreams.KMongo
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

interface PageStore {
    suspend fun putIfAbsent(newPage: Page): Boolean
    suspend fun get(name: Subdomain): Page?
    suspend fun getAll(): List<Page>
    suspend fun remove(name: Subdomain): Page?
    suspend fun removeAllOlderThan(time: Instant)
}

class InMemoryPageStore : PageStore {
    private val map = ConcurrentHashMap<Subdomain, PageModel>()

    override suspend fun getAll() = map.values.sortedBy { it.created }.map(PageModel::fromModel).toList()

    override suspend fun putIfAbsent(newPage: Page): Boolean {
        return map.putIfAbsent(newPage.subdomain, newPage.toModel()) == null
    }

    override suspend fun get(name: Subdomain) = map[name]?.fromModel()

    override suspend fun remove(name: Subdomain) = map.remove(name)?.fromModel()

    override suspend fun removeAllOlderThan(time: Instant) {
        map.filter { it.value.created < time }.forEach {
            map.remove(it.key)
        }
    }
}

//TODO: Some caching will most likely be necessary
class MongoDBPageStore(connectionString: String) : PageStore {
    private val client = KMongo.createClient(connectionString).coroutine

    //TODO: Parametrize
    private val database = client.getDatabase("isthisafuckingjoke")
    private val collection = database.getCollection<PageModel>("pages")

    override suspend fun putIfAbsent(newPage: Page): Boolean {
        val pageFromDb = collection.findOne(PageModel::subdomain eq newPage.subdomain)
        if (pageFromDb != null) return false
        collection.insertOne(newPage.toModel())
        return true
    }

    override suspend fun get(name: Subdomain) = collection.findOne(PageModel::subdomain eq name)?.fromModel()

    override suspend fun getAll() = collection.find().sort(descending(PageModel::created)).toList().map(PageModel::fromModel)

    override suspend fun remove(name: Subdomain) = collection.findOneAndDelete(PageModel::subdomain eq name)?.fromModel()

    override suspend fun removeAllOlderThan(time: Instant) {
        collection.deleteMany(PageModel::created lt time)
    }

}

data class PageModel(
    val type: String,
    val subdomain: Subdomain,
    val created: Instant = Instant.now(),
    val forwardData: ForwardPage.ForwardContent? = null,
    val basicTemplateContent: BasicTemplatePage.BasicTemplateContent? = null,
    val richTemplateContent: RichTextTemplatePage.RichTextTemplateContent? = null
) {
    fun fromModel(): Page = when (type) {
        ForwardPage::class.simpleName -> ForwardPage(subdomain, forwardData ?: TODO())
        BasicTemplatePage::class.simpleName -> BasicTemplatePage(subdomain, basicTemplateContent ?: TODO())
        RichTextTemplatePage::class.simpleName -> RichTextTemplatePage(subdomain, richTemplateContent ?: TODO())
        else -> TODO()
    }
}

//TODO: Maybe replace with inline class?
typealias Subdomain = String

